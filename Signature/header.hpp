#ifndef _HEADER_
#define _HEADER_

#include<iostream>
#include<cstdlib>
#include<queue>
#include<string>
#include<sstream>
#include<memory> 
#include<algorithm>
#include<iomanip>
#include<functional>
#include<mutex>
#include<atomic>
#include<exception>
#include<stdexcept>
#include<openssl/md5.h>
#include<boost/filesystem.hpp>
#include<boost/filesystem/fstream.hpp>

using std::cout;
using std::endl;
using namespace boost::filesystem;
using func = std::function<void()>;

class threads_joiner
{
private:
	std::vector<std::thread> &threads;
public:
	explicit threads_joiner(std::vector<std::thread> & thr) 
		: threads(thr) {}

	~threads_joiner()
	{
		for (unsigned long i = 0; i < threads.size();++i)
		{
			if (threads[i].joinable())
				threads[i].join();
		}
	}
};

class work_queue
{
private:
	mutable std::mutex mt;
	std::queue<func> w_queue;
	std::condition_variable cVar;
public:
	work_queue() {}
	void push(func new_val)
	{
		std::lock_guard<std::mutex> lg(mt);
		w_queue.push(std::move(new_val));
		cVar.notify_one();
	}
	void wait_pop(func &val)
	{
		std::unique_lock<std::mutex> ulock(mt);
		cVar.wait(ulock, [this] {return !w_queue.empty(); });
		val = std::move(w_queue.front());
		w_queue.pop();
	}
	std::shared_ptr<func> wait_pop()
	{
		std::unique_lock<std::mutex> lk(mt);
		cVar.wait(lk, [this] {return !w_queue.empty(); });
		auto res(std::make_shared<func>(std::move(w_queue.front())));
		w_queue.pop();
		return res;
	}
	bool try_pop(func &val)
	{
		std::lock_guard<std::mutex> lg(mt);
		if (w_queue.empty())
			return false;
		val = std::move(w_queue.front());
		w_queue.pop();
		return true;
	}
	std::shared_ptr<func> try_pop()
	{
		std::lock_guard<std::mutex> lg(mt);
		if (w_queue.empty())
			return std::shared_ptr<func>();
		auto res(std::make_shared<func>(std::move(w_queue.front())));
		w_queue.pop();
		return res;
	}

	bool empty() const
	{
		std::lock_guard<std::mutex> lg(mt);
		return w_queue.empty();
	}
};

class thread_pool
{
private:
	std::atomic_bool done;
	work_queue wQueue;
	std::vector<std::thread> threads;
	threads_joiner joiner;

	void worker_thread()
	{
		while(!done)
		{
			func task;
			if(wQueue.try_pop(task))
			{
				task();
			}
			else
			{
				std::this_thread::yield();
			}
		}
	}

public:
	thread_pool() : done(false), joiner(threads)
	{
		auto th = std::thread::hardware_concurrency();
		try
		{
			for (auto i = 0; i < th;++i)
			{
				threads.push_back(std::thread(&thread_pool::worker_thread, this));
			}
		}
		catch(...)
		{
			done = true;
			throw;
		}
	}
	~thread_pool()
	{
		done = true;
	}

	template<class funcType, class... args>
	void add_to_pool(funcType f,args... a)
	{
		wQueue.push(std::bind(f, a...));
	}
};

////
class file_handler
{
private:
	long long bufSize;
	std::mutex myMutex;
	std::queue<std::pair<long long, std::unique_ptr<char[]>>> data_queue;
	std::condition_variable condVar;

public:
	explicit file_handler(long long b) : bufSize(b) {}
	
	auto hash(const char *data,long long bSize)
	{
		unsigned char c[MD5_DIGEST_LENGTH];
		MD5_CTX mdContext;
		MD5_Init(&mdContext);
		MD5_Update(&mdContext, data, bSize);
		MD5_Final(c, &mdContext);
		std::stringstream temp;
		std::for_each(std::begin(c), std::end(c), [&](char elem)
		{
			temp << std::hex << std::setfill('0') << std::setw(2) << (elem & 0xFF);
		});
		return temp.str();
	}

	void read_block(ifstream &ifs,long long i)
	{
		std::unique_ptr<char[]> ptr(new char[bufSize]);
		ifs.seekg(i*bufSize, std::ios::beg);
		ifs.read(ptr.get(), bufSize);
		std::lock_guard<std::mutex> lk(myMutex); //lock to push
		data_queue.push(std::make_pair(i,std::move(ptr)));
		condVar.notify_one();
	}
	void write_block(ofstream &ofs)
	{
		std::unique_lock<std::mutex> ulock(myMutex);
		condVar.wait(ulock, [this] {return !data_queue.empty(); });
		auto tp = std::move(data_queue.front());
		data_queue.pop();
		ulock.unlock();   //unlock mutex to calculate hash
		auto t_hash = hash(tp.second.get(), bufSize);
		ulock.lock();    //lock to write
		//cout << "thread #" << std::this_thread::get_id() << " --- " << tp.first << endl;;
		ofs.seekp(tp.first * (t_hash.size() + 1), std::ios::beg);
		ofs << t_hash << '\n';
	}
	bool isEmpty()
	{
		std::lock_guard<std::mutex> lg(myMutex);
		return data_queue.empty();
	}
};

#endif _HEADER_


