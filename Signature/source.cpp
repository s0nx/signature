#include"header.hpp"
#include<boost/algorithm/string/predicate.hpp>
#include<boost/filesystem.hpp>
#include<boost/filesystem/fstream.hpp>

using std::cout;
using std::endl;
using namespace boost::filesystem;

auto input_parse(char *input)
{
	std::string temp(input);
	long long bSize{};
	if (std::all_of(temp.begin(), temp.end(), ::isdigit))
	{
		bSize = std::stoi(temp);
	}
	if (boost::algorithm::contains(temp, "kb")
		|| boost::algorithm::contains(temp, "KB")
		|| boost::algorithm::contains(temp,"Kb"))
	{
		bSize = std::stoi(temp);
		bSize *= 1024;
	}
	if (boost::algorithm::contains(temp, "mb")
		|| boost::algorithm::contains(temp, "MB")
		|| boost::algorithm::contains(temp,"Mb"))
	{
		bSize = std::stoi(temp);
		bSize *= 1024 * 1024;
	}
	return bSize;
}

int main(int argc, char *argv[])
{
	
	if(argc < 3)
	{
		cout << "usage: signature [/path/to/input/file] [/path/to/output/file] [blocksize: 8kb,1MB,128...default = 1MB]" << endl;
		return 1;
	}
	
	try
	{
		//x201 
		//path p_in("C:/Users/zepp/Source/Repos/signature/Signature/city.jpg");
		//path p_out("C:/Users/zepp/Source/Repos/signature/Signature/out.sig");
		//w520 
		//path p_in("C:/test/video.mp4");
		//path p_out("C:/test/out.sig");
		path p_in(argv[1]);
		path p_out(argv[2]);

		if(!is_regular_file(p_in))
		{
			std::cerr << p_in << " is not a regular file!\n";
			exit(EXIT_FAILURE);
		}
		if (exists(p_in))
			cout << "file " << p_in << " exists" << endl;
		boost::filesystem::ifstream file;
		file.open(p_in, std::ios::in | std::ios::binary | std::ios::ate);
		boost::filesystem::ofstream outfile;
		outfile.open(p_out, std::ios::out | std::ios::binary);

		auto fileSize = file_size(p_in);
		if (fileSize == 0)
		{
			std::cerr << "file " << p_in << " is empty!" << endl;
			exit(EXIT_FAILURE);
		}

		cout << "file size: " << file_size(p_in) << " bytes" << endl;
		unsigned long long blockCount{};
		long long bufSize{};

		if(argc==3)
		{
			bufSize = 1024 * 1024;
		}
		else
			bufSize = input_parse(argv[3]); // chunk size

		cout << "buffer size: " << bufSize << " bytes" << endl;
		if (fileSize % bufSize != 0)
		{
			blockCount = fileSize / bufSize + 1;
		}
		else if (fileSize < bufSize)
		{
			blockCount = 1;
		}
		else
			blockCount = fileSize / bufSize;

		cout << "number of blocks: " << blockCount << endl;

		file_handler myfile(bufSize);
		
		try
		{
			thread_pool thr;

			for (int i = 0; i < blockCount; ++i)
			{
				thr.add_to_pool(&file_handler::write_block, &myfile, std::ref(outfile));
			}

			for (int i = 0; i < blockCount; ++i)
				myfile.read_block(file, i);
		}
		catch (std::system_error &ex)
		{
			std::cerr << "system error: " << ex.what();
		}

	}

	catch (const boost::filesystem::filesystem_error &e)
	{
		std::cerr << e.what() << '\n';
	}
	catch(...)
	{
		std::cerr << "unknown exception caught\n";
	}
}